using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

public class IK : MonoBehaviour
{
    public List<Transform> Leg;
    private List<Vector3> initLeg = new List<Vector3>() ;
    private List<Vector3> lastPos = new List<Vector3>();
    public float Speed = 15;
    public  Vector3 offset;
    public float stepSize;
    public float multiplier;
    private Vector3 multiplied;
    private bool firstTime = true;
    private List<int> queued = new List<int>();
    private List<float> queuedDist = new List<float>();
    private List<bool> movingLeg = new List<bool>();
    private Rigidbody rb;



    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        for(int i = 0; i <Leg.Count/2; i++){
            queued.Add(0);
            queuedDist.Add(0);
            movingLeg.Add(false);
        }
        for (int i = 0; i<Leg.Count; i++){
            initLeg.Add(Vector3.zero);
            lastPos.Add(Vector3.zero);
            initLeg[i] = Leg[i].transform.localPosition;
            lastPos[i] = Leg[i].position;
            
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i<Leg.Count; i++) {
            Leg[i].position = lastPos[i];
            if(Vector3.Distance(RayDown(i), lastPos[i]) > stepSize ){
                Queue(i);
            }
        }
        if(queued[0] > 2) queued[0] =2;
        if(queued[1] > 2) queued[1] =2;
        CheckLegs();
        for(int i=0; i<queued.Count; i++){
            if(queued[i] > 0){
                queuedDist[i] = AverageDistance(i*2);
            }
            Debug.Log(i);
        }
    }

    float AverageDistance(int i){
        float Average = 0;
        List<float> LDist = new List<float>();
        for(int j=0;j<Leg.Count/2; j++){
            LDist.Add(Vector3.Distance(lastPos[i+j], RayDown(i+j)));
        }
        Average = LDist.Average();
        return Average;
    }

    void Queue(int i){
        if(i < Leg.Count/2){
            if(queued[1] == 0)
                Mathf.Clamp(queued[0]+=1,0,2);
        }
        else{
            if(queued[0] == 0)
                Mathf.Clamp(queued[1]+=1,0,2);
            
        }        
    }

    
    void CheckLegs(){
        if(queuedDist[0] > queuedDist[1]){
            if(movingLeg[0] == false){
                for(int l=0; l<Leg.Count/2; l++){
                    MoveLeg(l);
                    queued[0] = 0;
                    queuedDist[0] = 0;
                }
            }
        }
        else if(queuedDist[0] < queuedDist[1]){
            if(movingLeg[1] == false){
                for(int l=Leg.Count/2; l<Leg.Count; l++){
                    MoveLeg(l);
                    queued[1] = 0;
                    queuedDist[1] = 0;
                } 
            }
        }
    }
    
    

    async void MoveLeg(int i){
        movingLeg[i/2] = true;
        while(Vector3.Distance(RayDown(i), lastPos[i]) > 0.1f) {
            lastPos[i] = Vector3.MoveTowards(lastPos[i], RayDown(i), Speed*Time.deltaTime);
            await Task.Yield();
        }
        await Task.Delay(100);
        movingLeg[i/2] = false;
    }

    Vector3 RayDown(int i){
        RaycastHit hit;
        multiplied = new Vector3(transform.position.x,transform.position.y + multiplier,transform.position.z );
        if (Physics.Raycast(initLeg[i] + multiplied,
            -Vector3.up, out hit,
            Mathf.Infinity)){
            return hit.point;
        }
        else{
            return lastPos[i];
        }
    }

    private void OnDrawGizmos() {
        //RaycastHit hit;
        Gizmos.color = Color.red;
        /*for (int i=0; i < initLeg.Count; i++) {
            if (Physics.Raycast(initLeg[i] + multiplied,
                Vector3.down, out hit,
                Mathf.Infinity))
            {
                    Gizmos.DrawSphere(hit.point, 0.075f);
            }*/
            for (int i=0; i < initLeg.Count; i++) {
                    Gizmos.DrawSphere(RayDown(i), 0.075f);
        }
    }

}
